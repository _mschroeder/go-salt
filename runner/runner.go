package runner

import "gitlab.com/_mschroeder/go-salt"

type RunnerService interface {
}

type RunnerServiceImpl struct {
	client *salt.Client

	Manage ManageService
}

var _ RunnerService = &RunnerServiceImpl{}

func New(c *salt.Client, options ...func(*RunnerServiceImpl)) (*RunnerServiceImpl, error) {
	r := &RunnerServiceImpl{}
	r.client = c

	r.Manage = &ManageServiceImpl{Client: c}

	for _, option := range options {
		option(r)
	}

	return r, nil
}
