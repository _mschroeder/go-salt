package runner

import (
	"gitlab.com/_mschroeder/go-salt"
)

type ManageService interface {
	GetPresent() ([]string, error)
}

type ManageServiceImpl struct {
	Client *salt.Client
}

var _ ManageService = &ManageServiceImpl{}

func (m *ManageServiceImpl) GetPresent() ([]string, error) {

	var s []string
	if err := m.Client.CallSaltRun(&s, []string{"manage.present"}...); err != nil {
		return nil, err
	}

	return s, nil
}
