package salt

import (
	"bytes"
	"encoding/json"
	"log"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
)

type Client struct {

	// Salt Executable
	Salt string

	// Salt-Run Executable
	SaltRun string

	// Debug will send requests using test=True
	Debug bool

	DebugParam string

	// Path to executables
	Path string

	// Type of salt command output
	Output string

	// Target of salt command
	Target Target
}

func New(options ...func(*Client)) (*Client, error) {
	c := &Client{}
	c.Salt = "salt"
	c.SaltRun = "salt-run"
	c.Debug = false
	c.Path = "/usr/bin"
	c.Output = "json"
	c.DebugParam = "test=true"
	c.Target = GlobTarget{"*"}

	for _, option := range options {
		option(c)
	}

	return c, nil
}

func (c *Client) call(cmd *exec.Cmd, v interface{}) error {

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}

	if err := cmd.Start(); err != nil {
		return err
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(stdout)
	s := buf.String()
	s = strings.Replace(s, "ERROR: Minions returned with non-zero exit code", "", -1)

	if err := cmd.Wait(); err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			// The program has exited with an exit code != 0

			// This works on both Unix and Windows. Although package
			// syscall is generally platform dependent, WaitStatus is
			// defined for both Unix and Windows and in both cases has
			// an ExitStatus() method with the same signature.
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				log.Printf("Exit Status: %d", status.ExitStatus())
			}
		} else {
			log.Printf("cmd.Wait: %v", err)
		}
	}

	if err := json.NewDecoder(strings.NewReader(s)).Decode(v); err != nil {
		return err
	}

	return nil
}

func (c *Client) CallSalt(target Target, v interface{}, args ...string) error {

	args = c.GetArgs(args)
	tArgs := target.GetTargetArgs()

	args = append(tArgs, args...)

	cmd := exec.Command(c.GetSaltPath(), args...)
	if err := c.call(cmd, v); err != nil {
		return err
	}

	return nil
}

func (c *Client) CallSaltRun(v interface{}, args ...string) error {
	args = c.GetArgs(args)
	cmd := exec.Command(c.GetSaltRunPath(), args...)
	if err := c.call(cmd, v); err != nil {
		return err
	}

	return nil
}

func (c *Client) GetArgs(args []string) []string {

	out := []string{"--out", c.Output}
	args = append(args, out...)

	if c.Debug {
		args = append(args, c.DebugParam)
	}

	return args
}

func (c *Client) GetSaltPath() string {
	return filepath.Join(c.Path, c.Salt)
}

func (c *Client) GetSaltRunPath() string {
	return filepath.Join(c.Path, c.SaltRun)
}

func (c *Client) SetTarget(t Target) {
	c.Target = t
}
