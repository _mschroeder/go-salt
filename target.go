package salt

import "fmt"

type Target interface {
	GetTargetArgs() []string
}

type GlobTarget struct {
	Selector string
}

type GrainTarget struct {
	Selector string
}

type CompoundTarget struct {
	Selector string
}

func (t GlobTarget) GetTargetArgs() []string {
	return []string{fmt.Sprintf("%s", t.Selector)}
}

func (t GrainTarget) GetTargetArgs() []string {
	return []string{"-G", fmt.Sprintf("%s", t.Selector)}
}

func (t CompoundTarget) GetTargetArgs() []string {
	return []string{"-C", fmt.Sprintf("%s", t.Selector)}
}
