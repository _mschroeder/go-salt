package module

import (
	"gitlab.com/_mschroeder/go-salt"
)

type GrainsService interface {
	Get(req GrainsGetRequest) (map[string]interface{}, error)
}

type GrainsServiceImpl struct {
	Client *salt.Client
}

var _ GrainsService = &GrainsServiceImpl{}

func (m *GrainsServiceImpl) Get(req GrainsGetRequest) (map[string]interface{}, error) {

	args := []string{"grains.get"}
	reqArgs, err := req.GetArgs()
	if err != nil {
		return nil, err
	}
	args = append(args, reqArgs...)

	var resp map[string]interface{}

	if err := m.Client.CallSalt(m.Client.Target, &resp, args...); err != nil {
		return nil, err
	}

	return resp, nil
}
