package module

import "gitlab.com/_mschroeder/go-salt"

type ModuleService interface {
}

type ModuleServiceImpl struct {
	client   *salt.Client
	Grains   GrainsService
	Services ServicesService
	PS       PSService
}

var _ ModuleService = &ModuleServiceImpl{}

func New(c *salt.Client, options ...func(*ModuleServiceImpl)) (*ModuleServiceImpl, error) {
	r := &ModuleServiceImpl{}
	r.client = c

	for _, option := range options {
		option(r)
	}

	r.Grains = &GrainsServiceImpl{Client: c}
	r.Services = &ServicesServiceImpl{Client: c}
	r.PS = &PSServiceImpl{Client: c}

	return r, nil
}
