package module

import (
	"gitlab.com/_mschroeder/go-salt"
)

type ServicesService interface {
	Available(service string) (bool, error)
	Status(req ServicesStatusRequest) (bool, error)
}

type ServicesServiceImpl struct {
	Client *salt.Client
}

var _ ServicesService = &ServicesServiceImpl{}

func (s *ServicesServiceImpl) Available(service string) (bool, error) {
	args := []string{"service.available"}
	args = append(args, service)

	var resp map[string]bool

	if err := s.Client.CallSalt(s.Client.Target, &resp, args...); err != nil {
		return false, err
	}

	for _, val := range resp {
		return val, nil
	}

	return false, nil
}

func (s *ServicesServiceImpl) Status(req ServicesStatusRequest) (bool, error) {
	args := []string{"service.status"}
	args = append(args, req.Service)
	if req.Signature != "" {
		args = append(args, req.Signature)
	}

	var resp map[string]bool

	if err := s.Client.CallSalt(s.Client.Target, &resp, args...); err != nil {
		return false, err
	}

	for _, val := range resp {
		return val, nil
	}

	return false, nil

}
